### Change Before Compile ###

* Change the dimension on line 19 of correct scale
* Change the FILE_NAME to qc files
* Change the SIZE_FILE to correct path of output the size information of every time step
* Change the WRITE_BACK to .TRUE. if you want the cloud information write back to the qc.nc

### Compile and Execute ###

```
#!bash
make
make test

```