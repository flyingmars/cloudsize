program cldsize

    implicit none
    include "netcdf.inc"

    Type :: cloud_para
        Integer :: time 
        Integer :: index_num
        Real(8) :: volume
        Real(8) :: max_height
        Real(8) :: min_height
        Real(8) :: qc_sum
        Real(8) :: buoy_sum
        Real(8) :: VKE_sum 
        Real(8) :: enstrophy 
        Real(8) :: enstrophy_z
        Real(8) :: center_x
        Real(8) :: center_y
        Real(8) :: center_z
    end Type

    Integer             , parameter :: NX=256  , NY=256  , NZ=70 , NT=2881
    Real(kind=8)        , parameter :: DX=250. , DY=250.
    Integer             , parameter :: GRIDSIZE   = NX*NY*NZ

    Character ( len=* ) , parameter :: OUT_PATH   = "/data2/flyingmars/cloud3d.nc"
    Character ( len=* ) , parameter :: VAR_NAME   = "cloud3d"

    Character ( len=* ) , parameter :: NC_FOLDER  = "../mjoshallow/"
    Character ( len=* ) , parameter :: NC_PNAME   = "mjoL."
    Character ( len=* ) , parameter :: NC_SUFFIX  = ".nc"
    
    Character ( len=* ) , parameter :: SIZE_FILE  = "cloud_3d.bin"

    Logical             , parameter :: WRITE_BACK = .FALSE.
    Integer             , parameter :: CONNECT    = 6
    Integer             , parameter :: C_DIM      = 3 
  
    Integer                         :: ncidout , varout
    Integer                         :: Time , x , label , temp
    Integer                         :: label_data(NX,NY,NZ)
    
    Integer(kind=8)                 :: reclen

    Type ( cloud_para )             :: record(GRIDSIZE)
    Real(kind=8)                    :: rho(NZ)

    inquire ( iolength=reclen ) record(1) 
    open( &
        unit=4567 ,&
        file=SIZE_FILE ,&
        form="unformatted"  ,&
        status="unknown" ,&
        access="direct"  ,&
        recl=reclen &
    )
    
    call write_back_fcn ( 0 , Time , ncidout , varout , label_data , record )
    call get_rho ( rho ) 
    temp = 1
    do Time = 1 , NT
        write ( *,* ) 'Time = ' , Time
        call find_cloud( Time , label_data , record , label )     
        call write_back_fcn ( 1 , Time , ncidout , varout , label_data , record )

        do x  = 1 , label-1
            record(x) % time      = Time
            record(x) % index_num = x
            write ( 4567 , rec=temp ) record(x)
            temp = temp + 1
        enddo
    end do

    call write_back_fcn ( 2 , Time , ncidout , varout , label_data , record )

    close(4567)

contains

    subroutine check(status)
        integer, intent (in) :: status
    
        if(status /= nf_noerr) then 
            write(*,*) trim(nf_strerror(status))
            stop "Stopped"
        end if
    end subroutine check     

    subroutine get_rho( out_data )
        real(kind=8)      , intent (out) :: out_data(NZ)

        Character ( len=80 )             :: filename         
        Integer                          :: i
        
        filename = "./rho.txt" 
        
        open( unit=1357 , file=filename , status='unknown' , access='sequential')
        do i=1,NZ
            read( 1357 , * ) out_data( i )
        enddo
        close( 1357 )

    end subroutine

    subroutine get_zdim( out_data )
        real(kind=8)      , intent (out) :: out_data(NZ)

        Character ( len=80 )             :: filename
        Integer                          :: ncid , varnum
        Integer                          :: z_start(1) , z_count(1)

        filename = NC_FOLDER // NC_PNAME //  "w3d" // NC_SUFFIX

        z_start(1) = 1
        z_count(1) = NZ

        call check( nf_open( filename  , NF_NOWRITE , ncid ) )
        call check( nf_inq_varid( ncid , "zc" , varnum ) )
        call check( nf_get_vara_double( ncid , varnum , z_start , z_count ,out_data ) )
        call check( nf_close( ncid ) )
    end subroutine

    subroutine get_nc( para , varname , get_time , out_data )
        real(kind=8)         , intent (out) :: out_data(NX,NY,NZ)
        Character ( len=*  ) , intent (in)  :: para , varname 
        Integer              , intent (in)  :: get_time

        Character ( len=80 )             :: filename         
        Integer                          :: ncid , varnum
        Integer                          :: nc_start(4) , nc_count(4)
        filename = NC_FOLDER // NC_PNAME //  para // NC_SUFFIX  
        
        nc_start(1) = 1
        nc_start(2) = 1
        nc_start(3) = 1
        nc_start(4) = get_time
        nc_count(1) = NX
        nc_count(2) = NY
        nc_count(3) = NZ
        nc_count(4) = 1
        
        call check( nf_open( filename  , NF_NOWRITE , ncid ) )
        call check( nf_inq_varid( ncid , varname , varnum ) )
        call check( nf_get_vara_double( ncid , varnum , nc_start , nc_count , out_data ) )
        call check( nf_close( ncid ) )
    end subroutine

    subroutine write_back_fcn ( wb_state , cu_time , ncidout , varout , label_data , record )
        Integer             , intent(in)           :: label_data(NX,NY,NZ)
        Type ( cloud_para ) , intent(in)           :: record(GRIDSIZE)
        Integer             , intent(in)           :: cu_time , wb_state
        Integer             , intent(inout)        :: ncidout , varout
        
        Real(kind=8)                    :: refill_grid(NX,NY,NZ)
        Integer                         :: nc_start(4) , nc_count(4) , nc_dimid(4)
        
        if ( WRITE_BACK ) then
            if ( wb_state .eq. 0 ) then
                call check( nf_create( OUT_PATH , nf_clobber , ncidout ) )
                call check( nf_def_dim( ncidout , 'west_east'   , NX , nc_dimid(1) ) )
                call check( nf_def_dim( ncidout , 'south_north' , NY , nc_dimid(2) ) )  
                call check( nf_def_dim( ncidout , 'bottom_top'  , NZ , nc_dimid(3) ) )  
                call check( nf_def_dim( ncidout , 'Time'  , NF_UNLIMITED , nc_dimid(4) ) )  
                call check( nf_def_var( ncidout , VAR_NAME , NF_DOUBLE , 4 , nc_dimid , varout ) )
                call check( nf_enddef( ncidout ) )
            elseif ( wb_state .eq. 1 ) then
                nc_start(1) = 1
                nc_start(2) = 1
                nc_start(3) = 1
                nc_start(4) = cu_time
                nc_count(1) = NX
                nc_count(2) = NY
                nc_count(3) = NZ
                nc_count(4) = 1
                call refill_data( label_data , record , refill_grid )
                call check ( nf_put_vara_double( ncidout , varout , nc_start , nc_count , refill_grid ) )
            else
                call check( nf_close(ncidout) ) 
            endif
        end if

    end subroutine

    subroutine refill_data( label_data , rec_data , refill_grid )
        Integer          , intent (in)    :: label_data(NX,NY,NZ)
        Type(cloud_para) , intent (in)    :: rec_data(GRIDSIZE)
        real(kind=8)     , intent (out)   :: refill_grid(NX,NY,NZ)
        
        Integer                     :: x,y,z

        do x=1,NX
            do y=1,NY
                do z=1,NZ
                    if ( label_data(x,y,z) .EQ. 0 ) cycle
                    refill_grid(x,y,z)  =  real( rec_data(label_data(x,y,z)) %index_num )
                enddo
            enddo
        enddo
    end subroutine

    subroutine find_cloud( cu_time , label_data , rec_data , label )
        Integer         , intent (in)    :: cu_time

        Type(cloud_para), intent (out)   :: rec_data(GRIDSIZE)
        Integer         , intent (out)   :: label_data(NX,NY,NZ)
        Integer         , intent (out)   :: label
  
        real(kind=8)                :: inc , h_max , h_min , qc_sum , buoy_sum
        real(kind=8)                :: VKE_sum , enstrophy , enstrophy_z
        real(kind=8)                :: cur_vol , center_x  , center_y , center_z
        integer                     :: x , y , z , i , j , k , total_grid 
        integer                     :: search_flag(NX,NY,NZ)
        integer                     :: stackX(GRIDSIZE) , stackY(GRIDSIZE) , stackZ(GRIDSIZE)
        integer                     :: move_x_3d(6) =  (/ -1,  0, +1,  0,  0,  0 /),&
                                       move_y_3d(6) =  (/  0, -1,  0, +1,  0,  0 /),&
                                       move_z_3d(6) =  (/  0,  0,  0,  0, +1, -1 /),&
                                       move_x_2d(8)  = (/ -1,  0, +1,  0, -1, -1, +1, +1 /),&
                                       move_y_2d(8)  = (/  0, -1,  0, +1, -1, +1, -1, +1 /),&
                                       move_z_2d(8)  = (/  0,  0,  0,  0,  0,  0,  0,  0 /)
        integer                     :: cu_move , nex , ney , nez , myx , myy , myz 
        integer                     :: stack_ptr
        
        real(kind=8)                :: zc_data(NZ) 
        real(kind=8)                :: qc_data(NX,NY,NZ)     ,  &
                                       buoy_data(NX,NY,NZ)   ,  &
                                       th_data(NX,NY,NZ)     ,  &
                                       qv_data(NX,NY,NZ)     ,  &
                                       w_data(NX,NY,NZ)      ,  &
                                       xi_data(NX,NY,NZ)     ,  &
                                       eta_data(NX,NY,NZ)    ,  &
                                       zeta_data(NX,NY,NZ)

        real(kind=8)                :: avg_th , theta_p

        label = 1
        label_data(:,:,:)  = 0
        search_flag(:,:,:) = 0
        stack_ptr = 0
        stackX(:) = 0
        stackY(:) = 0
        stackZ(:) = 0

        call get_nc   ( "qc3d" , "qc"   , cu_time , qc_data )  
        call get_nc   ( "qv3d" , "qv"   , cu_time , qv_data )  
        call get_nc   ( "th3d" , "th"   , cu_time , th_data )  
        call get_nc   ( "w3d"  , "w"    , cu_time , w_data  )  
        call get_nc   ( "z3dx" , "xi"   , cu_time , xi_data    )  
        call get_nc   ( "z3dy" , "eta"  , cu_time , eta_data   )  
        call get_nc   ( "z3dz" , "zeta" , cu_time , zeta_data  )  
        call get_zdim ( zc_data )

        ! Calculate Buoy
        do k=1,NZ
            avg_th = 0.0
            do j=1,NY
                do i=1,NX
                    theta_p = th_data(i,j,k) * ( 1. + 0.608 * qv_data(i,j,k) - qc_data(i,j,k) )
                    avg_th  = avg_th + theta_p
                enddo
            enddo
            avg_th = avg_th / NX / NY
            do j=1,NY
                do i=1,NX
                    theta_p = th_data(i,j,k) * ( 1 + 0.608 * qv_data(i,j,k) - qc_data(i,j,k) )
                    buoy_data(i,j,k) = 9.81 * ( theta_p - avg_th ) / avg_th
                enddo
            enddo
        enddo

        ! Start Connecting Clouds
        do x = 1 , NX
            do y = 1 , NY
                do z =  1 , NZ
                    if ( search_flag(x,y,z) .EQ. 1 ) cycle

                    ! Initialize a new connected search
                    stackX(1) = x
                    stackY(1) = y
                    stackZ(1) = z
                    search_flag(x,y,z) = 1
                    stack_ptr = 1
                    
                    total_grid  = 0

                    inc         = 0.
                    h_max       = -1.
                    h_min       = zc_data(NZ) + 100.
                    qc_sum      = 0.0
                    buoy_sum    = 0.0
                    VKE_sum     = 0.0
                    enstrophy   = 0.0
                    enstrophy_z = 0.0
                    center_x    = 0.0
                    center_y    = 0.0
                    center_z    = 0.0
                    
                    do while ( stack_ptr .GE. 1 )

                        myx =  stackX(stack_ptr)
                        myy =  stackY(stack_ptr)
                        myz =  stackZ(stack_ptr)

                        stack_ptr = stack_ptr - 1
                        
                        !! Redefine the cloud defination here
                        if (                                               &
                               ( qc_data(myx,myy,myz)    .LT. 1E-5 )       &
                           ) cycle 
                        
                        label_data(myx,myy,myz) = label

                        !! Add current cloud with real volume and height and
                        !! other variable
                        total_grid = total_grid + 1

                        if ( myz .ne. 1 ) then
                            cur_vol = DX * DY * (zc_data(myz)-zc_data(myz-1))
                            if ( h_max .lt. zc_data(myz) ) h_max = zc_data(myz)
                            if ( h_min .gt. zc_data(myz) ) h_min = zc_data(myz)
                        else
                            cur_vol = DX * DY * (zc_data(2)-zc_data(1))
                            if ( h_max .lt. zc_data(2) ) h_max = zc_data(2)
                            if ( h_min .gt. zc_data(2) ) h_min = zc_data(2)
                        endif
                        inc         = inc        + cur_vol
                        qc_sum      = qc_sum     &
                                      + cur_vol * qc_data(myx,myy,myz) * rho(myz)
                        buoy_sum    = buoy_sum   + buoy_data(myx,myy,myz) ** 2
                        VKE_sum     = VKE_sum    + 0.5 * w_data(myx,myy,myz) ** 2
                        enstrophy   = enstrophy  & 
                                      + 0.5 * xi_data(myx,myy,myz)   ** 2 &
                                      + 0.5 * eta_data(myx,myy,myz)  ** 2 &
                                      + 0.5 * zeta_data(myx,myy,myz) ** 2 
                        enstrophy_z = enstrophy_z &
                                      + 0.5 * zeta_data(myx,myy,myz) ** 2 
                        center_x    = center_x + real(myx) 
                        center_y    = center_y + real(myy) 
                        center_z    = center_z + real(myz) 

                        ! override the current ptr
                        do cu_move = 1 , CONNECT
                            if ( C_DIM .EQ. 2 ) then
                                nex = mod(myx + move_x_2d( cu_move )+NX-1,NX) + 1
                                ney = mod(myy + move_y_2d( cu_move )+NY-1,NY) + 1
                                nez = myz + move_z_2d( cu_move )
                            else
                                nex = mod(myx + move_x_3d( cu_move )+NX-1,NX) + 1
                                ney = mod(myy + move_y_3d( cu_move )+NY-1,NY) + 1
                                nez = myz + move_z_3d( cu_move )
                            endif

                            !if ( nex .GT. NX   .OR.    ney .GT. NY    .OR.    nez .GT. NZ  ) cycle
                            !if ( nex .LT. 1    .OR.    ney .LT. 1     .OR.    nez .LT. 1   ) cycle
                            if ( nez .GT. NZ  .OR. nez .LT. 1    ) cycle
                            if ( search_flag(nex,ney,nez) .EQ. 1 ) cycle
                            stackX(stack_ptr + 1) = nex
                            stackY(stack_ptr + 1) = ney
                            stackZ(stack_ptr + 1) = nez
                            search_flag(nex,ney,nez) = 1
                            stack_ptr = stack_ptr + 1
                        end do
                    end do

                    if ( inc .GE. 1E-8  ) then
                        rec_data(label) % volume      = inc
                        rec_data(label) % max_height  = h_max 
                        rec_data(label) % min_height  = h_min 
                        rec_data(label) % qc_sum      = qc_sum
                        rec_data(label) % buoy_sum    = buoy_sum 
                        rec_data(label) % VKE_sum     = VKE_sum 
                        rec_data(label) % enstrophy   = enstrophy 
                        rec_data(label) % enstrophy_z = enstrophy_z
                        rec_data(label) % center_x    = center_x/real(total_grid)
                        rec_data(label) % center_y    = center_y/real(total_grid)
                        rec_data(label) % center_z    = center_z/real(total_grid)

                        label  = label + 1
                        inc         = 0.0
                        h_max       = 0.0
                        h_min       = 0.0
                        qc_sum      = 0.0
                        buoy_sum    = 0.0
                        VKE_sum     = 0.0
                        enstrophy   = 0.0
                        enstrophy_z = 0.0
                        center_x    = 0.0
                        center_y    = 0.0
                        center_z    = 0.0

                        total_grid  = 0
                    endif
                enddo
            enddo
        enddo


    end subroutine
end program

