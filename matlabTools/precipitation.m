function return_v = precipitation( plot_t )

    nc_info = load_info ;
    
    ncid  =  netcdf.open( nc_info.phys ,'NOWRITE');
    my    =  length(double(netcdf.getVar(ncid,1))) ;
    mx    =  length(double(netcdf.getVar(ncid,2))) ;

    varid = netcdf.inqVarID(ncid,'sprec') ;
    
    %qc = 0 ;
    for t = plot_t(1) : plot_t(2)
        %test = double( netcdf.getVar(ncid , varid , [0 0 t-1] , [mx my 1] ) ) ;
        if ( mod(t,100) == 0 )
            disp(['i=' num2str(t)]) ; 
        end                
        qc(t)  =  mean( mean( double( netcdf.getVar(ncid , varid , [0 0 t-1] , [mx my 1] ) ) , 2) , 1 ) ;
        qc(t)  = qc(t) * 60 * 60 * 1000 / 10000 * 10 ;  
    end
    
    plot(  [plot_t(1):plot_t(2)] ./ 144  , ...
          smooth( qc(plot_t(1):plot_t(2)) , 10 , 'moving') , ...
          'color', 'blue' , ...
          'lineWidth',2 ...
          );

    xlim( [plot_t(1) plot_t(2)] ./ 144);  
    %ylim([0 5]);

    
    xlabel('Days','FontSize',20);
    ylabel('Domain Averaged Percipitation Rate [mm/hr]','FontSize',20);
    title('CTime Series of Averaged Percipitation Rate','FontSize',20);
    set( gca , 'fontsize' , 15 ) ;

    netcdf.close(ncid);

    return_v = qc ;
end
