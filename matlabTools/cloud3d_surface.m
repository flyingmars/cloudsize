function return_v = cloud3d_surface( plot_t  )
    % Should specified only one time
    
    s_cloud = load_cloud ;
    [row ~] = size(s_cloud.Data);
    s_array   = s_cloud.Data ;
    
    nc_info = load_info ;
    ncid    =  netcdf.open( nc_info.cloud ,'NOWRITE');
    [~,mx] = netcdf.inqDim(ncid,0) ;
    [~,my] = netcdf.inqDim(ncid,1) ;
    [~,mz] = netcdf.inqDim(ncid,2) ;
    [~,mt] = netcdf.inqDim(ncid,3) ;

    ncid2    =  netcdf.open( nc_info.w ,'NOWRITE');
    z_index  =  netcdf.getVar(ncid2,1) ;
    ncid3    =  netcdf.open( nc_info.qc ,'NOWRITE');

    
    
    colors   = load_color ;
    
    index = 1 ;
    for i=1:row
        if ( mod(i,10000) == 0 )
            disp(['i=' num2str(i)]) ; 
        end
        
        if ( s_array(i).time ~= plot_t  )
            continue;
        end

		% record the cloudsize distribution
        cloudindex(index)         = s_array(i).index ;
        cloudvol(index)           = s_array(i).volume ;
        if (  s_array(i).min_height < 1000.0 && ( s_array(i).max_height - s_array(i).min_height ) > 200.0  )
            cloudpart(index)  = 1 ;
        else
            cloudpart(index)  = 0 ;
        end
        index = index + 1 ;
    end
    
    
    no_cloud_idx = index ;
    % Preprocessing 
    cloudconn  =  int32( round(double( netcdf.getVar(ncid , 0 , [0 0 0 plot_t-1] , [mx my mz 1] ) ) ) ) ;
    qc         =  double( netcdf.getVar(ncid3 , 4 , [0 0 0 plot_t-1] , [mx my mz 1] ) );
    for i=1:mx
        for j=1:my
            for k=1:mz
                if ( cloudconn(i,j,k) <= 0 | cloudconn(i,j,k) >= no_cloud_idx )
                    non_con(i,j,k) = 0.0 ;
                    con_cld(i,j,k) = 0.0 ;
                else
                    if ( cloudpart( cloudconn(i,j,k) ) == 0 )
                    	non_con(i,j,k) = qc(i,j,k) ;
                        con_cld(i,j,k) = 0.0 ;
                    else
                    	non_con(i,j,k) = 0.0 ;
                        con_cld(i,j,k) = qc(i,j,k) ;
                    end
                end
            end
        end
    end

    figure(1) ;
    p1 = patch( isosurface( ...
         ( [1 : mx] .* 250 - 125 ) ./ 1000 ...
        ,( [1 : my] .* 250 - 125 ) ./ 1000 ...
        , z_index ./ 1000 ...
        ,non_con ...
        ,1E-5 ...
    ));
    isonormals( ...
         ( [1 : mx] .* 250 - 125 ) ./ 1000 ...
        ,( [1 : my] .* 250 - 125 ) ./ 1000 ...
        , z_index ./ 1000 ...
        ,non_con ...
        ,p1   ...
    )    
    p2 = patch(isosurface( ...
         ( [1 : mx] .* 250 - 125 ) ./ 1000 ...
        ,( [1 : my] .* 250 - 125 ) ./ 1000 ...
        ,z_index ./ 1000 ...
        ,con_cld ...
        ,1E-5  ...
    ));
    isonormals( ...
         ( [1 : mx] .* 250 - 125 ) ./ 1000 ...
        ,( [1 : my] .* 250 - 125 ) ./ 1000 ...
        , z_index ./ 1000 ...
        ,con_cld ...
        ,p2   ...
    )        
    

    daspect([1 1 0.5]) ;
    view(3) ;
    camlight ;
    lighting gouraud ;
    
    netcdf.close(ncid);
    netcdf.close(ncid2) ;
    netcdf.close(ncid3) ;


    xlim(( [1 mx] .* 250 - 125 ) ./ 1000);
    ylim(( [1 my] .* 250 - 125 ) ./ 1000);
    zlim(  [0 7] );
    
    %legend('Clouds','Conevctive Clouds');
    %title(['Cloud water with Convective Cloud marked at Hour=' num2str(round(plot_t/6)) ],'FontSize',20);
    %xlabel('Zonal domain [km]','FontSize',20);
    %ylabel('Meridional domain [km]','FontSize',20);
    %zlabel('Height [km]')
    
    set(gca,'FontSize',15);
    set(p1,'FaceColor','b','EdgeColor','none');
    set(p2,'FaceColor','r','EdgeColor','none');

    grid on ;
    
    return_v = 1 ;
end
