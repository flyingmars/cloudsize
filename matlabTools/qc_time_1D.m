
function retrun_v = qc_time_1D( plot_t )

    nc_info = load_info ;

    ncid  =  netcdf.open( nc_info.qc ,'NOWRITE');
    mz    =  length(double(netcdf.getVar(ncid,1))) ;
    my    =  length(double(netcdf.getVar(ncid,2))) ;
    mx    =  length(double(netcdf.getVar(ncid,3))) ;

    %qc = 0 ;
    for t = plot_t(1) : plot_t(2)
        if ( mod(t,100) == 0 )
            disp(['i=' num2str(t)]) ; 
        end                
        qc(t)  =  mean( mean( mean( double( netcdf.getVar(ncid , 4 , [0 0 0 t-1] , [mx my mz 1] ) ) , 2) , 1 ) , 3 );
    end
    
    plot( [plot_t(1):plot_t(2)]    , ...
          qc(plot_t(1):plot_t(2)).*1E5  , ...
          'color', 'blue' , ...
          'lineWidth',2 ...
          );

    xlim([plot_t(1) plot_t(2)]);  
    %ylim([0 5]);

    
    xlabel('Output Number','FontSize',20);
    ylabel('Cloud Water Average [kg/kg 1E5]','FontSize',20);
    title('Cloud Water Average TimeSeries','FontSize',20);
    set( gca , 'fontsize' , 15 ) ;

    netcdf.close(ncid);

    return_v = 1 ;
end
