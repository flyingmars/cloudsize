function return_v = VKE_buoysqr_cloud_size( plot_t  )

    s_cloud = load_cloud ;
    [row ~] = size(s_cloud.Data);

    s_array   = s_cloud.Data ;
    
    acc_t = 1 ;
    for i=1:row
        if ( mod(i,10000) == 0 )
            disp(['i=' num2str(i)]) ; 
        end
        
        if ( s_array(i).time < plot_t(1) || s_array(i).time > plot_t(2) )
            continue;
        end
        
        if ( s_array(i).min_height > 1000.0 || ( s_array(i).max_height - s_array(i).min_height ) < 200.0 )
            continue;
        end
        
        
        cloudallw_sum(acc_t)     = s_array(i).VKE_sum  / ( s_array(i).volume / 1000000000 );
        cloudallbuoy_sum(acc_t)  = s_array(i).buoy_sum / ( s_array(i).volume / 1000000000 ) ;
        cloud_size(acc_t)        = s_array(i).volume / 1000000000 ;
        cloud_qc(acc_t)          = s_array(i).qc_sum / ( s_array(i).volume  ) * 1000 ;
       
        acc_t = acc_t + 1 ;
    end
   
    
    % plot pdf
    figure(1);
    scatter( [cloudallw_sum 600 600 600], [cloudallbuoy_sum 0.005 0.010 0.02] , ( [cloud_size 1 10 50 ]  ) .* 20 , [cloud_qc 0.7 0.7 0.7],'filled') ;
    xlim([0 800]);
    ylim([0 0.1]);
    colorbar();
    caxis([0 1.2]);

    
    title(['Cloud size and Water concentration '],'FontSize',20);
    xlabel('VKE [J * 1/km\^3]','FontSize',20);
    ylabel('Buoyancy [m^2/s\^4 * 1/km\^3]','FontSize',20);
    set(gca,'FontSize',15);
    set(gca,'color',[0 0 0]) ;
%     set(gca,'YScale','log')
    
    return_v = 1 ;
end
