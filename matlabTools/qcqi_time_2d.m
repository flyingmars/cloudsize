function retrun_v = qcqi_time_2d( plot_t )

    nc_info = load_info ;

    ncid        =  netcdf.open( nc_info.qc ,'NOWRITE');
    mz          =  length(double(netcdf.getVar(ncid,1))) ;
    my          =  length(double(netcdf.getVar(ncid,2))) ;
    mx          =  length(double(netcdf.getVar(ncid,3))) ;
    
    ncid2       =  netcdf.open( nc_info.qi ,'NOWRITE'); 
    
    ncid3       =  netcdf.open( nc_info.w ,'NOWRITE');
    z_index     =  netcdf.getVar(ncid3,1) ;
    

    for t = plot_t(1) : plot_t(2)
        if ( mod(t,100) == 0 )
            disp(['i=' num2str(t)]) ; 
        end
        temp_qc(1:mz)  =   mean( mean( double( netcdf.getVar(ncid  , 4 , [0 0 0 t-1] , [mx my mz 1] ) ) , 2) , 1 ) ;
        temp_qi(1:mz)  =   mean( mean( double( netcdf.getVar(ncid2 , 4 , [0 0 0 t-1] , [mx my mz 1] ) ) , 2) , 1 ) ;
        qcqi(1:mz,t)   =   temp_qc(1:mz) + temp_qi(1:mz) ;
    end
    
    %qcqi(qcqi < 1E-5 ) = NaN ;
    contourf( [plot_t(1):plot_t(2)]./144 , z_index ./ 1000 ,qcqi(1:mz,plot_t(1):plot_t(2)) .* 1000);
    shading flat;
    colorbar ;
    %caxis([0.01 0.2]);
    xlabel('Time [Day]','FontSize',20);
    ylabel('Height [km]','FontSize',20);
    title('Cloud Water + Cloud Ice Domain Average [g/kg]','FontSize',20);
    
    set( gca , 'fontsize' , 15 ) ;
    
    xlim( [plot_t(1) plot_t(2)]./ 144 ); 
    ylim( [z_index(1)  z_index(60)] ./ 1000 ) ;
    
    netcdf.close(ncid);
    netcdf.close(ncid2);
    
    return_v = 1 ;
end
