function return_v = VKE_buoysqr( plot_t  )

    s_cloud = load_cloud ;
    [row ~] = size(s_cloud.Data);

    s_array   = s_cloud.Data ;
    
    acc_t = 1 ;
    for i=1:row
        if ( mod(i,10000) == 0 )
            disp(['i=' num2str(i)]) ; 
        end
        
        if ( s_array(i).time < plot_t(1) || s_array(i).time > plot_t(2) )
            continue;
        end
        
        if ( s_array(i).min_height > 1000.0 || ( s_array(i).max_height - s_array(i).min_height ) < 200.0 )
            continue;
        end
        
        
        cloudallw_sum(acc_t)     = s_array(i).VKE_sum  / ( s_array(i).volume / 1000000000 );
        cloudallbuoy_sum(acc_t)  = s_array(i).buoy_sum / ( s_array(i).volume / 1000000000 ) ;
        acc_t = acc_t + 1 ;
    end


    % hist3 will bin the data
    xi = linspace( 0 , 800 , 50);
    yi = linspace( 0 , 0.1  , 50);
    hst = hist3([cloudallw_sum' cloudallbuoy_sum'],{xi yi});

    % normalize the histogram data
    dx = xi(2)-xi(1);
    dy = yi(2)-yi(1);
    area = dx*dy;
    pdfData = hst/sum(sum(hst));%/area;

    % plot pdf
    figure(1);
    pcolor(xi,yi,log10(pdfData)');
    xlim([0 800]);
    ylim([0 0.1]);
    colorbar();
    caxis([-5 0]);
    shading flat
    %plot(cloudallw_sum,cloudallbuoy_sum,'.','color','red');
    
    title(['VKE vs Buoyancy^2'],'FontSize',20);
    xlabel('VKE [J * 1/km\^3]','FontSize',20);
    ylabel('Buoyancy [m^2/s\^4 * 1/km\^3]','FontSize',20);
    set(gca,'FontSize',15);
%     set(gca,'YScale','log')
    
    return_v = 1 ;
end
