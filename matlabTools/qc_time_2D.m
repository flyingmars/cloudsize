
function retrun_v = qc_time_2D( plot_t )

    nc_info = load_info ;

    ncid  =  netcdf.open( nc_info.qc ,'NOWRITE');
    mz    =  length(double(netcdf.getVar(ncid,1))) ;
    my    =  length(double(netcdf.getVar(ncid,2))) ;
    mx    =  length(double(netcdf.getVar(ncid,3))) ;

    qc = 0 ;
    for t = plot_t(1) : plot_t(2)
        if ( mod(t,100) == 0 )
            disp(['i=' num2str(t)]) ; 
        end        
        qc(1:mz,t)  =  mean( mean( double( netcdf.getVar(ncid , 4 , [0 0 0 t-1] , [mx my mz 1] ) ) , 2) , 1 ) ;
    end
    
    pcolor([plot_t(1):plot_t(2)],1:mz,qc(1:mz,plot_t(1):plot_t(2)).*1E6);
    shading flat ;
    set(gca, 'CLim', [0, 10]);
    colorbar ;
    
    xlabel('OutputNumber','FontSize',20);
    ylabel('Height[m]','FontSize',20);
    title('Cloud Water Average','FontSize',20);
    set( gca , 'fontsize' , 15 ) ;
    xlim([plot_t(1) plot_t(2)]); 
    
    netcdf.close(ncid);

    return_v = qc ;
end
