
function return_v = draw(filename , format )
    if ( strcmp( format , 'eps' ) )
        print('-depsc2','-r200',['./graph/' filename]);
    else
        print('-dpng','-r50',['./graph/' filename]);
    end
    return_v = 1 ;
end