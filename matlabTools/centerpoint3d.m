function return_v = centerpoint3d( plot_t  )

    s_color = load_color ;
    s_cloud = load_cloud ;
    [row ~] = size(s_cloud.Data);

    s_array   = s_cloud.Data ;

    index = 1 ;
    cu_time = 0 ;
    for i=1:row
        if ( mod(i,10000) == 0 )
            disp(['i=' num2str(i)]) ; 
        end
        
        if ( s_array(i).time < plot_t(1) || s_array(i).time > plot_t(2) )
            continue;
        end
        
        if ( cu_time ~= s_array(i).time )
            index = 1 ; 
            cloudnum(s_array(i).time) = 0 ;
        end
        cu_time = s_array(i).time ;
        

       % record the convective cloud 
        if ( s_array(i).volume < 1.5 * 1000000000 )
            continue;
        end

		% record the cloudsize distribution
        cloudnum(cu_time)         = cloudnum(cu_time) + 1 ;
        center_x(cu_time,index)   = s_array(i).center_x ;
        center_y(cu_time,index)   = s_array(i).center_y ;
        center_z(cu_time,index)   = s_array(i).center_z ;
        volume(cu_time,index)     = s_array(i).volume / 1000000000  ;    
		qc_sum(cu_time,index)     = s_array(i).qc_sum ;% / ( s_array(i).volume  ) * 1000 ;
        index = index + 1 ;

    end


    % plot pdf
    figure(1) ;
    clf ;

    for time = plot_t(1) : plot_t(2)
        scatter( center_x(time,1:cloudnum(time)) , center_y(time,1:cloudnum(time)) , ... %center_z(time,1:cloudnum(time)) , ...
                 qc_sum(time,1:cloudnum(time) ) ./ 5000 , ... 
                 s_color.red(mod(time - plot_t(1),5)+1,:) , ...
                 'filled' ...
               );
        xlim([0 256]);
        ylim([0 256]);
        %zlim([0 40 ]);
        %caxis([0 1.1]);
        %colorbar();
        hold on ;
    end
    

    title(['Cloud Center Distribution'],'FontSize',20);
    xlabel('x grid index','FontSize',20);
    ylabel('y grid index ','FontSize',20);
    %zlabel('z grid index ','FontSize',20);
    set(gca,'FontSize',15);
%     set(gca,'YScale','log')
    
    return_v = 1 ;
end
