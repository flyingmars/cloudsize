function return_v = histogram_overlap_qc(plot_t)
    % plot_t{1} = [1  10]
    % plot_t{2} = [11 20] ..etc
    
    if ( nargin < 1 )
        plot_t{1} = [346 489] ;
        plot_t{2} = [490 650] ;
        plot_t{3} = [651 788] ;
        plot_t{4} = [789 930] ;
        plot_t{5} = [931 1079];
    end
    
    
    colors    = load_color ;

    len = length ( plot_t ) ;
    
    for i=1:len
        temp_hist = histogram_qc(plot_t{i}) ;
        figure(2) 
        plot(   temp_hist{1,2} , temp_hist{1,1}  , ...
                'lineWidth',1.2, ...
                'color',colors.red(i,:) ...
            );
        hold on ;
        figure(2) 
        plot(   temp_hist{2,2} , temp_hist{2,1}  , ...
                'lineWidth',1.2, ...
                'color',colors.blue(i,:) ...
            );
        set(gca,'YScale','log');
        set(gca,'XScale','log');
        hold on ;
    end

    set(gca,'YScale','log');
    set(gca,'XScale','log');

    title( ['Cloud Water Histogram' ] , 'FontSize' ,20);
    xlabel('Qc sum[g * 1/m\^3 ]','FontSize',20);
    ylabel('Occurence [m\^-3 min\^-1]','FontSize',20);
    xlim([1E-3 10^(0.5)])
    ylim([1E-14 1E-13 ])
    set(gca,'FontSize',15);    

    return_v = 1;
end
