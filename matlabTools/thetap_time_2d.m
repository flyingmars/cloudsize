
function retrun_v = thetap_time_2d( plot_t )

    nc_info = load_info ;

    ncid  =  netcdf.open( nc_info.th ,'NOWRITE');
    mz    =  length(double(netcdf.getVar(ncid,1))) ;
    my    =  length(double(netcdf.getVar(ncid,2))) ;
    mx    =  length(double(netcdf.getVar(ncid,3))) ;
    
    ncid2    =  netcdf.open( nc_info.w ,'NOWRITE');
    z_index  =  netcdf.getVar(ncid2,1) ;
    
    th_init(1:mz) = mean( mean( double( netcdf.getVar(ncid , 4 , [0 0 0 0] , [mx my mz 1] ) ) , 2) , 1 ) ;

    for t = plot_t(1) : plot_t(2)
        if ( mod(t,100) == 0 )
            disp(['i=' num2str(t)]) ; 
        end
        temp(1:mz)  =   mean( mean( double( netcdf.getVar(ncid , 4 , [0 0 0 t-1] , [mx my mz 1] ) ) , 2) , 1 ) ;
        th(1:mz,t)  =   temp(1:mz) - th_init ;
    end
    
    contourf( [plot_t(1):plot_t(2)]./144 , z_index ./ 1000 ,th(1:mz,plot_t(1):plot_t(2)));
    shading flat ;
    %set(gca, 'CLim', [0, 10]);
    colorbar ;
    
    xlabel('Time [Day]','FontSize',20);
    ylabel('Height [km]','FontSize',20);
    title('Theta difference [K]','FontSize',20);
    set( gca , 'fontsize' , 15 ) ;
    
    xlim( [plot_t(1) plot_t(2)]./ 144 ); 
    %ylim( [z_index(2) : z_index(mz) ./ 1000 ) ;
    
    netcdf.close(ncid);
    netcdf.close(ncid2);
    
    return_v = 1 ;
end
