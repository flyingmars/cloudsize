function return_v = qc_enstrophy( plot_t  )

    s_cloud = load_cloud ;
    [row ~] = size(s_cloud.Data);

    s_array   = s_cloud.Data ;
    
    acc_t = 1 ;
    for i=1:row
        if ( mod(i,10000) == 0 )
            disp(['i=' num2str(i)]) ; 
        end
        
        if ( s_array(i).time < plot_t(1) || s_array(i).time > plot_t(2) )
            continue;
        end
        
        if ( s_array(i).min_height > 1000.0 || ( s_array(i).max_height - s_array(i).min_height ) < 200.0 )
            continue;
        end
        
        
        cloudall_ens(acc_t)    = s_array(i).enstrophy / ( s_array(i).volume / 1000000000 ) ;
        cloudallqc_sum(acc_t)  = s_array(i).qc_sum / ( s_array(i).volume  ) * 1000 ;
        acc_t = acc_t + 1 ;
    end

    % hist3 will bin the data
    xi = linspace( 0 , 1.0 , 50);
    yi = linspace( 0 , 0.15  , 50);
    hst = hist3([cloudallqc_sum' cloudall_ens'],{xi yi});

    % normalize the histogram data
    dx = xi(2)-xi(1);
    dy = yi(2)-yi(1);
    area = dx*dy;
    pdfData = hst/sum(sum(hst));%/area;

    % plot pdf
    figure(1);
    pcolor(xi,yi,log10(pdfData)');
    xlim([0 1.0]);
    ylim([0 0.15]);
    colorbar();
    caxis([-7 0]);
    shading flat
    %plot(cloudallqc_sum,cloudall_ens,'.','color','red');


    title(['Enstrophy vs Qc'],'FontSize',20);
    xlabel('Qc sum[g * 1/m\^3 ]','FontSize',20);
    ylabel('Enstrophy [1/s * 1/km\^3]','FontSize',20);
    set(gca,'FontSize',15);
%     set(gca,'YScale','log')
    
    return_v = 1 ;
end
