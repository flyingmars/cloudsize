function return_v = volume_buoyAvg( plot_t  )

    s_cloud = load_cloud ;
    [row ~] = size(s_cloud.Data);

    s_array   = s_cloud.Data ;
    
    acc_t = 1 ;
    for i=1:row
        if ( mod(i,10000) == 0 )
            disp(['i=' num2str(i)]) ; 
        end
        
        if ( s_array(i).time < plot_t(1) || s_array(i).time > plot_t(2) )
            continue;
        end
        
        if ( s_array(i).min_height > 1000.0 || ( s_array(i).max_height - s_array(i).min_height ) < 200.0 )
            continue;
        end
        
        
        cloudall_size(acc_t)     = s_array(i).volume / 1000000000 ;
        cloudallbuoy_sum(acc_t)  = s_array(i).buoy_sum / ( s_array(i).volume / 1000000000 ) ;
        acc_t = acc_t + 1 ;
    end


    disp('ready to draw B and W');
    scatter  ( cloudallbuoy_sum , ...
               cloudall_size    , ...
               10 , 'filled'    , ...
               'MarkerEdgeColor' , [0.0 0.0 1.0] , ...
               'MarkerFaceColor' , [0.0 0.0 1.0]  ...
             );

    title(['Average Buoyancy vs Average W'],'FontSize',20);
    xlabel('Buoyancy [m/s\^2 x 1/km\^3]','FontSize',20);
    ylabel('volume [km\^3]','FontSize',20);
    set(gca,'FontSize',15);
    set(gca,'YScale','log')
    
    return_v = 1 ;
end
