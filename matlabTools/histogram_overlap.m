function return_v = histogram_overlap(plot_t)
    % plot_t{1} = [1  10]
    % plot_t{2} = [11 20] ..etc
    
    if ( nargin < 1 )
        plot_t{1} = [346 489] ;
        plot_t{2} = [490 650] ;
        plot_t{3} = [651 788] ;
        plot_t{4} = [789 930] ;
        plot_t{5} = [931 1079];
    end
    
    
    colors    = load_color ;

    len = length ( plot_t ) ;
    
    ini_hist = histogram(plot_t{1}) ;
    for i=1:4:len
        temp_hist = histogram(plot_t{i}) ;
        figure(2)         
        plot(   temp_hist{2,2} , sign( temp_hist{2,1} - ini_hist{2,1}) .* log( round(abs( temp_hist{2,1} - ini_hist{2,1}))+2 )  , ...
                'lineWidth',2, ...
                'color',colors.red(i,:) ...
            );
        hold on ;
        
        figure(3)
        plot(   temp_hist{2,2} ,  temp_hist{2,1}   , ...
                'lineWidth',2, ...
                'color',colors.red(i,:) ...
            );        
        %hold on ;
        %figure(2) 
        %plot(   temp_hist{2,2} , temp_hist{2,1}  , ...
        %        'lineWidth',1.2, ...
        %        'color',colors.blue(i,:) ...
        %    );
        %set(gca,'YScale','log');
        %set(gca,'XScale','log');
        hold on ;
    end

    %set(gca,'YScale','log');
    figure(2) 
    set(gca,'XScale','log');
    title( ['Convective Cloud Size Histogram Difference' ] , 'FontSize' ,20);
    xlabel('Size [km\^3]','FontSize',20);
    ylabel('Log of Occurence [clouds / hour]','FontSize',20);
    xlim([1E-2 1E2])
    ylim([-4 3])
    set(gca,'FontSize',15);    
    
    figure(3)
    set(gca,'XScale','log');
    set(gca,'YScale','log');
    title( ['Convective Cloud Size Histogram' ] , 'FontSize' ,20);
    xlabel('Size [km\^3]','FontSize',20);
    ylabel('Occurence [clouds / hour]','FontSize',20);
    xlim([1E-2 1E2])
    ylim([1E0 1E3])
    set(gca,'FontSize',15);       
    
    return_v = 1;
end
