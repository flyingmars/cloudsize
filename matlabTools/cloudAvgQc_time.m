function return_v = cloudAvgQc_time( plot_t )
    s_cloud   = load_cloud ;
    [row col] = size(s_cloud.Data);
    s_array   = s_cloud.Data ;
    
    acc_t = 0 ;
    for i=1:row

        if ( mod(i,10000) == 0 )
            disp(['i=' num2str(i)]) ; 
        end

        while ( acc_t ~= s_array(i).time )
            acc_t = acc_t + 1 ;
            cloud_num(acc_t) = 0 ;
            cloudqc_sum(acc_t) = 0 ;
        end

        if ( acc_t < plot_t(1) || acc_t > plot_t(2) )
            continue;
        end

        if ( s_array(i).min_height > 1000.0 || ( s_array(i).max_height - s_array(i).min_height ) < 200.0 )
            continue;
        end    

        cloud_num(acc_t)   = cloud_num(acc_t) + 1 ;
        cloudqc_sum(acc_t) = cloudqc_sum(acc_t) + s_array(i).qc_sum ;

    end

    acc_t = 0 ;
    for i=1:row
        if ( mod(i,10000) == 0 )
            disp(['i=' num2str(i)]) ; 
        end

        while ( acc_t ~= s_array(i).time )
            acc_t = acc_t + 1 ;
            cloudall_num(acc_t) = 0 ;
            cloudallqc_sum(acc_t) = 0 ;
        end

        if ( acc_t < plot_t(1) || acc_t > plot_t(2) )
            continue;
        end

        cloudall_num(acc_t)   = cloudall_num(acc_t) + 1 ;
        cloudallqc_sum(acc_t) = cloudallqc_sum(acc_t) + s_array(i).qc_sum ;
    end


    disp('ready to draw qcAvgQc');

    hold on
    plot  ( [plot_t(1):plot_t(2)] ./ 144, ...
            cloudqc_sum(plot_t(1):plot_t(2)) ./ cloud_num(plot_t(1):plot_t(2)) , ...
            'color','red', ...
            'LineWidth',2 ...
          );
    plot  ( [plot_t(1):plot_t(2)] ./ 144, ...
            cloudallqc_sum(plot_t(1):plot_t(2)) ./  cloudall_num(plot_t(1):plot_t(2)) , ...
            'color','blue', ...
            'LineWidth',2 ...
        );  
    title(['Cloud Average Qc'],'FontSize',20);
    xlabel('Time [Day]','FontSize',20);
    ylabel('Cloud Avg Qc [kg/kg]','FontSize',20);
    xlim([plot_t(1) plot_t(2)] ./144 );
    set(gca,'FontSize',15);
    %set(gca,'YScale','log')
    
    return_v = 1 ;
end
