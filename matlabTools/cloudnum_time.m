function return_v = cloudnum_time( plot_t  )

    s_cloud = load_cloud ;
    [row ~] = size(s_cloud.Data);

    s_array   = s_cloud.Data ;
    
    numall    = zeros(plot_t(2),1) ;
    num2_20   = zeros(plot_t(2),1) ;
    num02_2   = zeros(plot_t(2),1) ;
    num002_02   = zeros(plot_t(2),1) ;
    
    colors    = load_color ;
    for i=1:row
        if ( mod(i,10000) == 0 )
            disp(['i=' num2str(i)]) ; 
        end
        
        if ( s_array(i).time < plot_t(1) || s_array(i).time > plot_t(2) )
            continue;
        end
        
        % recordnumber of all cloud size  
        cu_time          = s_array(i).time ;
        numall(cu_time)  = numall(cu_time)   + 1 ;


		% record the convective cloud 
        if ( s_array(i).volume > 2 * 1000000000 && s_array(i).volume < 20 * 1000000000 )
           num2_20(cu_time)  = num2_20(cu_time)   + 1 ;
        end
        if ( s_array(i).volume > 0.2 * 1000000000 && s_array(i).volume < 2 * 1000000000 )
           num02_2(cu_time)  = num02_2(cu_time)   + 1 ;
        end
        if ( s_array(i).volume > 0.02 * 1000000000 && s_array(i).volume < 0.2 * 1000000000 )
           num002_02(cu_time)  = num002_02(cu_time)   + 1 ;
        end        
    end


    % plot pdf
    figure(1);
    clf ;
    hold on ;
    %plot( [plot_t(1):plot_t(2)] ./ 144 , smooth( numall( plot_t(1):plot_t(2))   , 10 , 'moving' ) ,'b' ,'LineWidth' ,2  ) ;
    plot( [plot_t(1):plot_t(2)] ./ 144 , smooth( num2_20(plot_t(1):plot_t(2))   , 10 , 'moving' ) ,'color',colors.red(5,:) ,'LineWidth' ,2  ) ;
    plot( [plot_t(1):plot_t(2)] ./ 144 , smooth( num02_2(plot_t(1):plot_t(2))   , 10 , 'moving' ),'color',colors.red(3,:) ,'LineWidth' ,2  ) ;
    plot( [plot_t(1):plot_t(2)] ./ 144 , smooth( num002_02(plot_t(1):plot_t(2))   , 10 , 'moving' ) ,'color',colors.red(1,:),'LineWidth' ,2  ) ;
    
    xlim( [plot_t(1) plot_t(2)] ./ 144 );
    
    
    title(['Cloud num'],'FontSize',20);
    xlabel('Day','FontSize',20);
    ylabel('Cloud numbers ','FontSize',20);
    set(gca,'FontSize',15);
%     set(gca,'YScale','log')
    
    return_v = 1 ;
end
