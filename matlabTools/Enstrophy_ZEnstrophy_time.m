function return_v = Enstrophy_ZEnstrophy_time( plot_t  )

    s_cloud = load_cloud ;
    [row ~] = size(s_cloud.Data);

    s_array   = s_cloud.Data ;
    
    ens   = zeros(plot_t(2),2) ;
    ens_z = zeros(plot_t(2),2) ;
    for i=1:row
        if ( mod(i,10000) == 0 )
            disp(['i=' num2str(i)]) ; 
        end
        
        if ( s_array(i).time < plot_t(1) || s_array(i).time > plot_t(2) )
            continue;
        end
        
        if ( s_array(i).min_height > 1000.0 || ( s_array(i).max_height - s_array(i).min_height ) < 200.0 )
            continue;
        end
        
        cu_time = s_array(i).time ;
        ens(cu_time,1)    = ens(cu_time,1)   + s_array(i).enstrophy   / ( s_array(i).volume / 1000000000 ) ;
        ens_z(cu_time,1)  = ens_z(cu_time,1) + s_array(i).enstrophy_z / ( s_array(i).volume / 1000000000 ) ;
        ens(cu_time,2)      = ens(cu_time,2)   + 1 ;
        ens_z(cu_time,2)    = ens_z(cu_time,2) + 1 ;

    end

    for t=plot_t(1):plot_t(2)
        ens(t,1)   = ens(t,1)    / ens(t,2) ;
        ens_z(t,1) = ens_z(t,1)  / ens_z(t,2) ;
        
    end

    % plot pdf
    clf;
    figure(1);
    plot( [ plot_t(1):plot_t(2) ] ./144, smooth( ens_z(plot_t(1):plot_t(2),1) ./ ens(plot_t(1):plot_t(2),1 ) , 10 , 'moving' ) ) ;
    xlim( [plot_t(1) plot_t(2)] ./144 );
    
    title(['0.5*zeta\^2 / Enstrophy '],'FontSize',20);
    xlabel(' Day ','FontSize',20);
    ylabel('0.5*zeta\^2 / Enstrophy ','FontSize',20);
    set(gca,'FontSize',15);
%     set(gca,'YScale','log')
    
    return_v = 1 ;
end
