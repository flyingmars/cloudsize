function return_v = volume_time( plot_t  )

    s_cloud = load_cloud ;
    [row ~] = size(s_cloud.Data);

    s_array   = s_cloud.Data ;
    
    vol    = zeros(plot_t(2),2) ;
    volc   = zeros(plot_t(2),2) ;
    vol25  = zeros(plot_t(2),2) ;
    vol50  = zeros(plot_t(2),2) ;
    vol25b = zeros(plot_t(2),2) ;
    vol50b = zeros(plot_t(2),2) ;

    colors = load_color ;
    
    index = 1 ;
    vol_percent = zeros(plot_t(2),3) ; % 25 50 75 
    for i=1:row
        if ( mod(i,10000) == 0 )
            disp(['i=' num2str(i)]) ; 
        end
        
        if ( s_array(i).time < plot_t(1) || s_array(i).time > plot_t(2) )
            continue;
        end
        % record all cloud size  
        cu_time = s_array(i).time ;
        vol(cu_time,1)    = vol(cu_time,1)   + ( s_array(i).volume / 1000000000 ) ;
        vol(cu_time,2)    = vol(cu_time,2)   + 1 ;

		% record the cloudsize distribution
        vol_record(index) = s_array(i).volume ;
        if ( i==row || s_array(i+1).time ~= cu_time )
             sort_cloud = sort( vol_record ) ;
             sort_len   = length(sort_cloud) ;
             vol_percent(cu_time,1) = sort_cloud(sort_len - round(sort_len/4)) ;
			 vol_percent(cu_time,2) = sort_cloud(sort_len - round(sort_len/2)) ;
			 vol_percent(cu_time,3) = sort_cloud(sort_len - round(sort_len/4 * 3) ) ;
             index = 1 ;
        else
             index = index + 1 ;
        end
        

		% record the convective cloud 
        if ( s_array(i).min_height > 1000.0 || ( s_array(i).max_height - s_array(i).min_height ) < 200.0 )
            continue;
        end
        
        volc(cu_time,1)    = volc(cu_time,1)   + ( s_array(i).volume / 1000000000 ) ;
        volc(cu_time,2)    = volc(cu_time,2)   + 1 ;

    end

    for i=1:row
        if ( mod(i,10000) == 0 )
            disp(['i=' num2str(i)]) ; 
        end
        
        if ( s_array(i).time < plot_t(1) || s_array(i).time > plot_t(2) )
            continue;
        end
        % record all cloud size  
        cu_time = s_array(i).time ;

		% record the convective cloud
        if ( s_array(i).volume < vol_percent(cu_time,2)  )
            vol50b(cu_time,1)    = vol50b(cu_time,1)   + ( s_array(i).volume / 1000000000 ) ;
            vol50b(cu_time,2)    = vol50b(cu_time,2)   + 1 ;
        end
        
        if ( s_array(i).volume < vol_percent(cu_time,3) )
            vol25b(cu_time,1)    = vol25b(cu_time,1)   + ( s_array(i).volume / 1000000000 ) ;
            vol25b(cu_time,2)    = vol25b(cu_time,2)   + 1 ;
        end
        
        if ( s_array(i).volume > vol_percent(cu_time,2)  )
            vol50(cu_time,1)    = vol50(cu_time,1)   + ( s_array(i).volume / 1000000000 ) ;
            vol50(cu_time,2)    = vol50(cu_time,2)   + 1 ;
        end
        
        if ( s_array(i).volume > vol_percent(cu_time,1) )
            vol25(cu_time,1)    = vol25(cu_time,1)   + ( s_array(i).volume / 1000000000 ) ;
            vol25(cu_time,2)    = vol25(cu_time,2)   + 1 ;
        end
        
    end

    for t=plot_t(1):plot_t(2)
        vol(t,1)    = vol(t,1)     / vol(t,2)   ;
        volc(t,1)   = volc(t,1)    / volc(t,2)  ;     
        vol25(t,1)  = vol25(t,1)   / vol25(t,2) ;     
        vol50(t,1)  = vol50(t,1)   / vol50(t,2) ;     
        vol25b(t,1)  = vol25b(t,1)   / vol25b(t,2) ;     
        vol50b(t,1)  = vol50b(t,1)   / vol50b(t,2) ;          
    end

    % plot volume evolution
    figure(1);
    clf ;
    hold on ;
    plot( [plot_t(1):plot_t(2)] ./ 144 , smooth( vol(plot_t(1):plot_t(2),1)   , 10 , 'moving' ) ,'color',colors.blue(3,:) ,'LineWidth',2 ) ;
    plot( [plot_t(1):plot_t(2)] ./ 144 , smooth( volc(plot_t(1):plot_t(2),1)  , 10 , 'moving' ) ,'r','LineWidth',2  ) ;
    plot( [plot_t(1):plot_t(2)] ./ 144 , smooth( vol25(plot_t(1):plot_t(2),1) , 10 , 'moving' ) ,'color',colors.blue(5,:),'LineWidth',2  ) ;
    plot( [plot_t(1):plot_t(2)] ./ 144 , smooth( vol50(plot_t(1):plot_t(2),1) , 10 , 'moving' ) ,'color',colors.blue(4,:),'LineWidth',2  ) ;
    plot( [plot_t(1):plot_t(2)] ./ 144 , smooth( vol50b(plot_t(1):plot_t(2),1) , 10 , 'moving' ),'color',colors.blue(2,:),'LineWidth',2  ) ;
    plot( [plot_t(1):plot_t(2)] ./ 144 , smooth( vol25b(plot_t(1):plot_t(2),1) , 10 , 'moving' ),'color',colors.blue(1,:),'LineWidth',2  ) ;    
    
    xlim( [plot_t(1) plot_t(2)] ./ 144 ) ;      
    title(['Volume Average'],'FontSize',20);
    xlabel('Day','FontSize',20);
    ylabel('Volume [km\^3] ','FontSize',20);
    set(gca,'FontSize',15);
%     set(gca,'YScale','log')

    % plot preicptation
    figure(2);
    prec = precipitation(plot_t) ;
    clf ;
    [ax,h1,h2] = plotyy([plot_t(1):plot_t(2)] ./144 , smooth(vol(plot_t(1):plot_t(2),1),10,'moving') , ...
                        [plot_t(1):plot_t(2)] ./144 , smooth(prec(plot_t(1):plot_t(2)),10,'moving')    ...
                       );
    xlim(ax(1),[plot_t(1) plot_t(2)] ./ 144);
    xlim(ax(2),[plot_t(1) plot_t(2)] ./ 144);
    set( ax(2),'XTickLabel',[]);
    set( ax(1),'YTick', linspace(0,2,21) ) ;
    set( ax(2),'YTick', linspace(0,0.1,21) ) ;
    title(['Domain Averaged Volume and Precipitation'],'FontSize',20);
    xlabel(ax(1) , 'Day','FontSize',20);
    ylabel(ax(1) , 'Averaged Volume [km\^3] ','FontSize',20);
    ylabel(ax(2) , 'Averaged Precipitation [mm/hr] ','FontSize',20);
    set(h1,'color',[96 167 214] ./ 255 ) ;
    set(h2,'color',[222 104 54] ./ 255 ) ;
    set(ax,{'ycolor'},{[96 167 214] ./ 255 ;[222 104 54] ./ 255 })
    set(gca,'FontSize',15);
    
    return_v = 1 ;
end
