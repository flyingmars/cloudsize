function return_v = core_histogram(plot_t,color)

    s_cloud   = load_cloud ;
    [row ~] = size(s_cloud.Data);
    s_array   = s_cloud.Data ;

    disp('handle core');
    acc_t = 0 ;
    cloudcore_num  = 0;
    cloud_num      = 0;
    cidx = 1 ;
    cloudcore_size(1)=0;
    for i=1:row
        if ( mod(i,10000) == 0 )
            disp(['i=' num2str(i)]) ; 
        end   
        while ( acc_t ~= s_array(i).time )
            acc_t = acc_t + 1 ;
        end

        if ( acc_t < plot_t(1) || acc_t > plot_t(2) )
            continue;
        end

        
        cloud_num = cloud_num + 1 ;
        
        if ( s_array(i).min_height > 1000.0 || ( s_array(i).max_height - s_array(i).min_height ) < 200.0 )
            continue;
        end   
        
        cloudcore_size(cidx) = s_array(i).volume / 1000000000 ;
        cloudcore_num = cloudcore_num + 1 ;
        cidx = cidx + 1 ;
    end

    disp('ready to draw histgram');

    [n, xout] = hist( cloudcore_size ,logspace(-3,3,20));
    n = n ./ cloud_num ;
    n(n<=1E-7) = 1E-7 ;
    plot(  xout , n  , ...
           'lineWidth',2, ...
           'color',color ...
        );
    set(gca,'YScale','log');
    set(gca,'XScale','log');

    title( ['Convective Cloud Size Histogram' ] , 'FontSize' ,20);
    xlabel('Size [km^3]','FontSize',20);
    ylabel('Fraction over all cloud','FontSize',20);
    xlim([1E-3 1E4])
    ylim([1E-7 1])
    set(gca,'FontSize',15);    

    return_v = 1 ;
end
