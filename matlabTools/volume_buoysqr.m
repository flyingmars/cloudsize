function return_v = volume_buoysqr( plot_t  )

    s_cloud = load_cloud ;
    [row ~] = size(s_cloud.Data);

    s_array   = s_cloud.Data ;
    
    acc_t = 1 ;
    for i=1:row
        if ( mod(i,10000) == 0 )
            disp(['i=' num2str(i)]) ; 
        end
        
        if ( s_array(i).time < plot_t(1) || s_array(i).time > plot_t(2) )
            continue;
        end
        
        if ( s_array(i).min_height > 1000.0 || ( s_array(i).max_height - s_array(i).min_height ) < 200.0 )
            continue;
        end
        
        
        cloudall_size(acc_t)     = s_array(i).volume / 1000000000 ;
        cloudallbuoy_sum(acc_t)  = s_array(i).buoy_sum / ( s_array(i).volume / 1000000000 ) ;
        acc_t = acc_t + 1 ;
    end

    % hist3 will bin the data
    xi = linspace( 0 , 1 , 20);
    yi = linspace( 0 , 200  , 20);
    hst = hist3([cloudall_size' cloudallbuoy_sum'],{xi yi});

    % normalize the histogram data
    dx = xi(2)-xi(1);
    dy = yi(2)-yi(1);
    area = dx*dy;
    pdfData = hst/sum(sum(hst))/area;

    % plot pdf
    figure(1);
    [C,h] = contour(xi,yi,pdfData,'color','red','LevelList',logspace(-6,0,7));
    xlim([0 3]);
    ylim([0 100]);
    %caxis([1E-5 1]);
    clabel(C,h) ;    
    

%     disp('ready to draw B and W');
%     scatter  ( cloudallbuoy_sum , ...
%                cloudall_size    , ...
%                10 , 'filled'    , ...
%                'MarkerEdgeColor' , [0.0 0.0 1.0] , ...
%                'MarkerFaceColor' , [0.0 0.0 1.0]  ...
%              );

    title(['Volume vs Buoyancy Square'],'FontSize',20);
    xlabel('Buoyancy [m/s\^2 x 1/km\^3]','FontSize',20);
    ylabel('volume [km\^3]','FontSize',20);
    set(gca,'FontSize',15);
%     set(gca,'YScale','log')
    
    return_v = 1 ;
end
