function return_v = histogram_qc(plot_t)

    s_cloud   = load_cloud ;
    [row ~] = size(s_cloud.Data);
    s_array   = s_cloud.Data ;

    disp('handle cloud');
    acc_t = 0 ;
    cloud_num  = 0;
    cidx = 1 ;
    cloud_size(1)=0;
    for i=1:row
        if ( mod(i,10000) == 0 )
            disp(['i=' num2str(i)]) ; 
        end
        
        cu_time = s_array(i).time ;
        
        if ( cu_time < plot_t(1) || cu_time > plot_t(2) )
            continue;
        end

        cloud_size(cidx) = s_array(i).qc_sum / ( s_array(i).volume  ) * 1000 ;
        cloud_num = cloud_num + 1 ;
        cidx = cidx + 1 ;
    end

    disp('handle core');
    acc_t = 0 ;
    cloudcore_num  = 0;
    cidx = 1 ;
    cloudcore_size(1)=0;
    for i=1:row
        if ( mod(i,10000) == 0 )
            disp(['i=' num2str(i)]) ; 
        end   

        cu_time = s_array(i).time ;
        
        if ( cu_time < plot_t(1) || cu_time > plot_t(2) )
            continue;
        end

        if ( s_array(i).min_height > 1000.0 || ( s_array(i).max_height - s_array(i).min_height ) < 200.0 )
            continue;
        end    

        cloudcore_size(cidx) = s_array(i).qc_sum / ( s_array(i).volume  ) * 1000 ;
        cloudcore_num = cloudcore_num + 1 ;
        cidx = cidx + 1 ;
    end
    
    figure(1);
    hold on ;
    disp('ready to draw histgram');
    
    all_domain = 250 * 256 * 250 * 256 * 19098 * ( plot_t(2) - plot_t(1) ) * 10 ;
    
    [n, xout] = hist( cloud_size , logspace(-3,0.5,20) );
    n = n ./ all_domain ;
    n(n<=1E-14) = 1E-14 ;
    figure(1);
    plot(  xout , n  , ...
           'lineWidth',2, ...
           'color','blue' ...
        );
    
    return_v{1,1} = n ;
    return_v{1,2} = xout ;
    
    [n, xout] = hist( cloudcore_size ,logspace(-3,0.5,20));
    n = n ./ all_domain ;
    n(n<=1E-14) = 1E-14 ;
    figure(1);
    plot(  xout , n  , ...
           'lineWidth',2, ...
           'color','red' ...
        );
    
    return_v{2,1} = n ;
    return_v{2,2} = xout ;
    
    set(gca,'YScale','log');
    set(gca,'XScale','log');

    %title( ['Cloud Size Histogram #' num2str(plot_t(1)) ' to #' num2str(plot_t(2)) ] , 'FontSize' ,20);
    xlabel('Qc sum[g * 1/m\^3 ]','FontSize',20);
    ylabel('Occurence [m\^-3 min\^-1]','FontSize',20);
    xlim([1E-3 10^(0.5)])
    ylim([1E-14 1E-13 ])
    
    %set(gca,'XTick',logspace(-2,3,7) );
    %set(gca,'XTickLabel',{'-2','-1','0','1','2','3'});
    set(gca,'FontSize',15);    

end
