clear all ;
clc ;
clf ;

Path       = ['/data2/mog/VVM/DATA/mjoshallow4'];
qname      = ['/q1mjo.dat'];


plevel = [1000, 950, 925, 900, 850, 800, 700, 600, 500, 400, 300, 250, 200, 150, 100];
time   = [0:147] ;

fp = fopen([Path qname],'r');


qmat = fread(fp,[length(plevel) length(time)],'float');


fclose(fp) ;

% Draw Graph

fig = figure(1) ;

pcolor( time ./4 , ...
        plevel , ...
        qmat ...
      );
shading flat ;
colorbar()
title('Q1 [K/day]' , 'fontsize',20);
xlabel('Time [Day]' ,'fontsize',20 );
ylabel('Pressure level[hPa]','fontsize',20);
set(gca,'fontsize',15);
set(gca,'YDir','reverse');
print(fig,'-depsc2','-r200','../graph/MJOQ1Q2');