
IFT = ifort
NCDF = -L/opt/netcdf4-intel/lib -lnetcdf -lnetcdff
ICDF = -I/opt/netcdf4-intel/include
OUT = cldsize.exe
OPT = -warn
all:
	${IFT} ${OPT} ${NCDF} ${ICDF}  cal_cloud_size.f90 -o ${OUT}
clean:
	clear
	rm ${OUT}
test:
	./${OUT}
